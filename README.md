# cdna_cupcake Singularity container
Bionformatics package cdna_cupcake<br>
cDNA_Cupcake is a miscellaneous collection of Python and R scripts used for analyzing sequencing data. Most of the scripts only require Biopython. For scripts that require additional libraries, it will be specified in documentation.
https://github.com/Magdoll/cDNA_Cupcake
cdna_cupcake Version: 12.5<br>

Singularity container based on the recipe: Singularity.cdna_cupcake_v12.5

Package installation using Miniconda3 V4.7.12<br>

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

can be pull (singularity version >=3.3) with:<br>
singularity pull cdna_cupcake_v12.5.sif oras://registry.forgemia.inra.fr/gafl/singularity/cdna_cupcake/cdna_cupcake:latest


